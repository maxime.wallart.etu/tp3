import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
        super.mount(element);
        document.querySelector('button').addEventListener('click',submit());
	}

	submit(event) {
        console.log(document.querySelector('input').getAttribute('name'));
    }
}
