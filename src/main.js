import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

document.querySelector('.newsContainer').setAttribute('style','');
function fermer(event) {
    document.querySelector('.newsContainer').setAttribute('style','display:none');
}
const close = document.querySelector('.closeButton');
close.addEventListener('click', fermer);

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
    { path: '/', page: pizzaList, title: 'La carte' },
    { path: '/a-propos', page: aboutPage, title: 'À propos' },
    { path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');